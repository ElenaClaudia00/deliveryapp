# DeliveryApp

**Steps**
        
    1. Initial steps  
        - run npm install to install dependencies
        - run node index.js to start the app

    2.  Database configuration, migrations and seed data
        - sqlite
            -  npx mkdir storage
            -  npx touch db.sqlite
            -  npx db browser

        - sequelize
            -  npx sequelize-cli init
        
        -  /config.json
            - delete test and prod
            - change "dialect"
            - add "storage"

        - first model
            - npx sequelize-cli model:generate --name ObjectName --attributes attr1:attr1_type,attr2:attr2_type,attr3:attr3_type
            - npx sequelize-cli db:migrate
            - npx sequelize-cli db:migrate:undo
            
        - seeding
            - npx sequelize-cli seed:generate --name demo-user
            - npx sequelize-cli db:seed:all
            - npx sequelize-cli db:seed:undo

    3.  Install graphQl
        - npm install express express-graphql graphql --save
        
    4.  Configure Altair GraphQL Client
        - add Chrome extension Altair GraphQL Client
        - add your application url (e.g. http://localhost:3000/graphql)

_**Stage 1**_

   **GraphQL Queries**

      - Get a restaurant by Id (PK) with all fields and the list of its categories.
        query GetRestaurant {
            restaurant(restaurantId:1) {
                id
                name
                minimumOrderAmount
                deliveryCosts
                deliveryTime
                description
                location
                rating
                categories {
                id
                name
                createdAt
                updatedAt
                }
                createdAt
                updatedAt
            } 
        }
    
      - Get a category by Id (PK) with all fields and the list of its products.
        query GetCategory {
            category(categoryId:1) {
                id
                name
                restaurant {
                id
                name
                minimumOrderAmount
                deliveryCosts
                deliveryTime
                description
                location
                rating
                createdAt
                updatedAt
                }
                products {
                id
                title
                description
                price
                quantity
                category {
                    id
                    name
                    createdAt
                    updatedAt
                }
                createdAt
                updatedAt
                }
                createdAt
                updatedAt
            }
        } 


 ****GraphQL Mutations****
 
      - Add categories for a selected restaurant.
        mutation AddCategory  {
          addCategories(categoriesInput: [{
            restaurantId: 1, 
            name: "Chicken and turkey dishes"
          }, 
            {
            restaurantId: 1, 
            name: "Grilled dishes"
            }]) {
            id
            name
            restaurant {
              id
              name
              minimumOrderAmount
              deliveryCosts
              deliveryTime
              description
              location
              rating
              createdAt
              updatedAt
            }
            products {
              id
              title
              description
              price
              quantity
              category {
                id
                name
                createdAt
                updatedAt
              }
              createdAt
              updatedAt
            }
            createdAt
            updatedAt
          }
        }

      - Add products for a selected category
        mutation AddProducts  {
          addProducts(productsInput: [{
            title: "Choco Pie",
            description : "Special pastry filled with Nutella cream, covered with powdered sugar",
            price: 14,
            quantity: 200 ,
            categoryId: 3
          },
            {
            title: "Chocolate souffle",
            description : "A warm cocoa muffin with creamy chocolate filling",
            price: 8.5,
            quantity: 250,
            categoryId: 3
          }
          ]) {
            id
            title
            description
            price
            quantity
            category {
              id
              name
              createdAt
              updatedAt
            }
            createdAt
            updatedAt
          }
        }

      - Add an order for an user with his selected items
        mutation AddOrder {
          addOrder(orderInput: {
            userId: 3,
            deliveryLocation: "Splaiul Independentei, nr. 204",
            orderProducts: [{
              productId: 1,
              numberOfItems: 3
            },
            {
              productId: 2,
              numberOfItems: 1
            }]
          }) {
            id
            orderCost
            deliveryLocation
            user {
              id
              email
              userName
              firstName
              lastName
              phone
              password
              createdAt
              updatedAt
            }
            createdAt
            updatedAt
          }
        }

