const { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLList } = require('graphql');
const models = require('../../models');

const categoryType = new GraphQLObjectType({
  name: 'CategoryDto',
  fields: () => ({
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
    /*se face un ciclu atunci cand se aduce
    un restaurant in obiectul categorie care deja este adus in lista categorii a restaurantului respectiv
    */
    // restaurant: { 
    //     type: restaurantType,
    //     resolve: async (parent, { restaurantId }) => {
    //       return await parent.getRestaurant();
    //     }
    // },
    createdAt: { type: GraphQLString },
    updatedAt: { type: GraphQLString },
  })
});

module.exports = categoryType;