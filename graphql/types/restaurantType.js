const { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLList, GraphQLFloat, GraphQLScalarType } = require('graphql');
const categoryType = require('./categoryDtoType');
const models = require('../../models');

const restaurantType = new GraphQLObjectType({
  name: 'Restaurant',
  fields: () => ({
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
    minimumOrderAmount: { type: GraphQLInt },
    deliveryCosts: { type: GraphQLFloat },
    deliveryTime: { type: GraphQLInt },
    description: { type: GraphQLString },
    location: { type: GraphQLString },
    rating: { type: GraphQLFloat },
    categories: {
      type: new GraphQLList(categoryType),
      resolve: async (parent) => {
        return await parent.getCategories();
      }
    },
    createdAt: { type: GraphQLString },
    updatedAt: { type: GraphQLString },

  })
});

module.exports = restaurantType;