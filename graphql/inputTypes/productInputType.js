const { GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLList, GraphQLSchema, GraphQLFloat } = require('graphql');


const productInputType = new GraphQLInputObjectType({
  name: 'ProductInput',
  fields: {
    title: { type: GraphQLString },
    description: { type: GraphQLString },
    price: {type: GraphQLFloat},
    quantity: {type: GraphQLFloat},
    categoryId: {type: GraphQLInt},
  }
});

module.exports = productInputType;