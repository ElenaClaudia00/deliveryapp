const { GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLList } = require('graphql');
const itemInputType = require("./itemInputType");

const orderInputType = new GraphQLInputObjectType({
  name: 'OrderInput',
  fields: {
    deliveryLocation: { type: GraphQLString },
    orderProducts: {type: GraphQLList(itemInputType)}
  }
});

module.exports = orderInputType;