'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    const categories = [
      {
        name: 'Pizza',
        restaurantId: 2, 
        createdAt: new Date(),
        updatedAt: new Date(),

      },
      {
        name: 'Pasta',
        restaurantId: 2, 
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Dessert',
        restaurantId: 2, 
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Sauces',
        restaurantId: 2, 
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Salads',
        restaurantId: 1, 
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Soups',
        restaurantId: 1, 
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Burgers',
        restaurantId: 1, 
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Pasta',
        restaurantId: 1, 
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];
    await queryInterface.bulkInsert('Categories', categories, {});
  },

  down: async (queryInterface, Sequelize) => {

    await queryInterface.bulkInsert('Categories', null, {});

  }
};
