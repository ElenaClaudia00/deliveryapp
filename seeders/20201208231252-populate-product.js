'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const products = [
      {
        title: 'Tasty Box Burger served with fries and BBQ sauce', 
        description: 'fresh black angus beef, minced and prepared after a special TASTY BOX receip, bacon, sesame bun, sauce, lettuce, onion, tomatoes, pickles, potatoes', 
        price: 29,
        quantity: 500, 
        categoryId: 7,
        createdAt: new Date(),
        updatedAt: new Date(),
        
      }, 
      {
        title: 'Classic beef burger served with fries and BBQ sauce', 
        description: 'fresh black angus beef, minced and prepared after a special TASTY BOX receip, sesame bun, sauce, lettuce, onion, tomatoes, pickle, potatoes', 
        price: 27,
        quantity: 500, 
        categoryId: 7,
        createdAt: new Date(),
        updatedAt: new Date(),
        
      }, 
      {
        title: 'Spinach and quinoa salad', 
        description: 'quinoa, spinach, tomatoes, baked bell pepper, soy sauce, mustard, greek pita', 
        price: 21,
        quantity: 350 , 
        categoryId: 5 ,
        createdAt: new Date(),
        updatedAt: new Date(),
        
      }, 
      {
        title: 'A la chef salad', 
        description: 'salad mix, spring onion, cucumbers, marinated beef meat, horseradish dressing, parmesan, greek pita', 
        price: 26,
        quantity: 400, 
        categoryId: 5,
        createdAt: new Date(),
        updatedAt: new Date(),
        
      }, 
      {
        title: 'Quattro Stagioni pizza', 
        description: 'Tomatoes sauce, Mozzarella cheese, ham, Pepperoni salami, mushrooms, bell pepper', 
        price: 24.5,
        quantity: 400, 
        categoryId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, 
      {
        title: 'Suprema pizza', 
        description: 'Tomatoes sauce, Mozzarella cheese, Pepperoni salami, beef, onion, mushrooms, bell pepper', 
        price: 25.5,
        quantity: 400, 
        categoryId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, 
      {
        title: 'Sweet Ketchup', 
        description: '', 
        price: 4,
        quantity: 55, 
        categoryId: 4,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, 
      {
        title: 'Spicy Ketchup', 
        description: '', 
        price: 4,
        quantity: 55, 
        categoryId: 4,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, 
      {
        title: 'Brownie', 
        description: 'A sweet cake with chocolate sauce and peanuts', 
        price: 30,
        quantity: 200, 
        categoryId: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, 
    ];
    await queryInterface.bulkInsert('Products', products, {});
  },

  down: async (queryInterface, Sequelize) => {
    
    await queryInterface.bulkDelete('Products', null, {});
    
  }
};
